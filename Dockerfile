FROM ubuntu:14.04.2
MAINTAINER Nigel Williams-Lucas
# Initial update
ENV UBUNTU_VERSION 14.04.2

LABEL OS.name="Ubuntu" OS.version="${UBUNTU_VERSION}"
LABEL OS.description="This is to serve as a base image."

RUN apt-get update

# This is to install add-apt-repository utility. All commands have to be non interactive with -y option
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y software-properties-common

# Install Oracle Java 8, accept license command is required for non interactive mode
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys EEA14886 && \
   DEBIAN_FRONTEND=noninteractive add-apt-repository -y ppa:webupd8team/java && \
   apt-get update && \
   echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | sudo /usr/bin/debconf-set-selections &&\
   DEBIAN_FRONTEND=noninteractive apt-get install -y oracle-java8-installer

# Install curl utility just for testing
RUN apt-get update && \
   DEBIAN_FRONTEND=noninteractive apt-get install -y curl wget

# Upgrade distro
RUN DEBIAN_FRONTEND=noninteractive apt-get upgrade -y

# grab gosu for easy step-down from root
RUN gpg --keyserver ha.pool.sks-keyservers.net --recv-keys B42F6819007F00F88E364FD4036A9C25BF357DD4
RUN curl -o /usr/local/bin/gosu -SL "https://github.com/tianon/gosu/releases/download/1.2/gosu-$(dpkg --print-architecture)" \
  && curl -o /usr/local/bin/gosu.asc -SL "https://github.com/tianon/gosu/releases/download/1.2/gosu-$(dpkg --print-architecture).asc" \
  && gpg --verify /usr/local/bin/gosu.asc \
  && rm /usr/local/bin/gosu.asc \
  && chmod +x /usr/local/bin/gosu

CMD ["bash"]